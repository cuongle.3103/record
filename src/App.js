import { useEffect, useState } from 'react'
import axios from 'axios'
import { Table, Card, Input, Button, Tag } from 'antd';
import moment from 'moment'
function App() {
  const [data, setData] = useState([])
  const [dataVM, setDataVM] = useState([])
  useEffect(() => {
    handleGetData()
    handleGetDataVM()
  }, [])
  const handleGetData = () => {
    axios.get(`http://203.171.21.129:3000/api/key`)
      .then(res => {
        console.log(res)
        setData(res.data)
      })
      .catch(error => console.log(error));
  }
  const handleGetDataVM = () => {
    axios.get(`http://203.171.21.129:3000/api/vm`)
      .then(res => {
        console.log(res)
        setDataVM(res.data)
      })
      .catch(error => console.log(error));
  }

  const COL = [
    {
      title: 'vmName',
      dataIndex: 'vmName',
      key: 'vmName',

    },
    {
      title: 'ads',
      dataIndex: 'ads',
      key: 'ads',
    },
    {
      title: 'paid',
      dataIndex: 'paid',
      key: 'paid',
    },
    {
      title: 'earning',
      dataIndex: 'earning',
      key: 'earning',
    },

    {
      title: 'folder',
      dataIndex: 'folder',
      key: 'folder',

    },
    {
      title: 'number',
      dataIndex: 'number',
      key: 'number',
    },
    {
      title: 'value',
      dataIndex: 'value',
      key: 'value',

    },
    {
      title: 'createdAt',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (data) => {
        return <p>{moment(data).format('YYYY-MM-DD HH:mm:ss')}</p>
      }
    },
  ]
  const COLVM = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',

    },
    {
      title: 'lastUpdate',
      dataIndex: 'lastUpdate',
      key: 'lastUpdate',
      render: (data) => {
        if (moment().diff(moment(data), 'minutes') > 5) {
          return <Tag color="#f50">{moment(data).format('YYYY-MM-DD HH:mm:ss')}</Tag>
        } else {
          return <Tag>{moment(data).format('YYYY-MM-DD HH:mm:ss')}</Tag>
        }
      }
    },

  ]

  console.log("ads", data.reduce((a, b) => a + (b['ads'] || 0), 0))

  const onSearch = (data) => {
    axios.get(`http://203.171.21.129:3000/api/key`, {
      params: {
        filter: { "where": { "vmName": data } }
      }
    }).then(res => {
      console.log(res)
      setData(res.data)
    }).catch(error => console.log(error));
  }

  return (
    <>
      <div className="App">
        <Card style={{ width: '100%' }}>
          <Button
            onClick={handleGetDataVM}
          >
            Load lại
          </Button>
        </Card>

        <Table columns={COLVM} dataSource={dataVM} bordered />
      </div>

      <div className="App">
        <Card style={{ width: '100%' }}>
          <p>Total ads: {data.reduce((a, b) => a + (b['ads'] || 0), 0)}</p>
          <p>Total paid: {data.reduce((a, b) => a + (b['paid'] || 0), 0)}</p>
          <p>Total earning: {data.reduce((a, b) => a + (b['earning'] || 0), 0)}</p>
          <p>Total expect: {data.reduce((a, b) => a + (b['expect'] || 0), 0)}</p>
        </Card>

        <Card style={{ width: '100%' }}>
          <Input.Search style={{ marginBottom: 15 }} placeholder="input search text" onSearch={onSearch} enterButton />
          <Button
            onClick={handleGetData}
          >
            Load lại
          </Button>
        </Card>

        <Table columns={COL} dataSource={data} bordered />
      </div>
    </>
  );
}

export default App;
